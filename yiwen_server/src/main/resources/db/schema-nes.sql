-- ----------------------------
-- Table structure for news_detail
-- ----------------------------
DROP TABLE IF EXISTS `news_detail`;
CREATE TABLE `news_detail` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `simple_id` bigint(20) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `time` datetime DEFAULT NULL,
  `news_from` varchar(255) DEFAULT NULL,
  `old_title` varchar(255) DEFAULT NULL,
  `content` text,
  PRIMARY KEY (`id`)
) ;

-- ----------------------------
-- Table structure for news_simple
-- ----------------------------
DROP TABLE IF EXISTS `news_simple`;
CREATE TABLE `news_simple` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) DEFAULT NULL,
  `docurl` varchar(255) DEFAULT NULL,
  `time` datetime DEFAULT NULL,
  `channelname` varchar(255) DEFAULT NULL,
  `imgurl` varchar(500) DEFAULT NULL,
  `has_content` BOOLEAN  NOT NULL DEFAULT FALSE,
  PRIMARY KEY (`id`)
);
