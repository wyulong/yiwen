package com.lnold.yiwen.service;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.OrderItem;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.lnold.yiwen.dao.SimpleMapper;
import com.lnold.yiwen.model.entity.Simple;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

@Service
public class SimpleService extends ServiceImpl<SimpleMapper, Simple> {

    @Autowired
    private SimpleMapper simpleMapper;

    public boolean existsSimple(String url) {
        QueryWrapper<Simple> queryWrapper = new QueryWrapper<Simple>().eq("docurl", url);
        return simpleMapper.exists(queryWrapper);
    }

    public Page<Simple> selectPage(String type, Integer pageNum, Integer pageSize) {

        Page<Simple> pageQuery = new Page<>();
        pageQuery.setPages(pageNum);
        pageQuery.setSize(pageSize);
        pageQuery.setOrders(OrderItem.descs("time"));

        return page(pageQuery, new LambdaQueryWrapper<Simple>()
                .eq(Simple::getChannelname, type)
                .eq(Simple::getHasContent, true));
    }


    public List<Simple> selectNoContent() {
        LambdaQueryWrapper<Simple> queryWrapper = new LambdaQueryWrapper<Simple>()
                .eq(Simple::getHasContent, false);
        return simpleMapper.selectList(queryWrapper);
    }

}
