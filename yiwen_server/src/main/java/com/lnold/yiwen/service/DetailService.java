package com.lnold.yiwen.service;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.lnold.yiwen.dao.DetailMapper;
import com.lnold.yiwen.model.entity.Detail;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class DetailService extends ServiceImpl<DetailMapper, Detail> {

    @Autowired
    private DetailMapper detailMapper;

    public Detail findBySimpleId(Long simpleId) {
        LambdaQueryWrapper<Detail> queryWrapper = new LambdaQueryWrapper<Detail>()
                .eq(Detail::getSimpleId, simpleId);
        return detailMapper.selectOne(queryWrapper);
    }
}
