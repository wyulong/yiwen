package com.lnold.yiwen.util;

import cn.hutool.core.collection.CollectionUtil;
import cn.hutool.core.date.DateException;
import cn.hutool.core.date.DateUtil;
import cn.hutool.core.util.StrUtil;
import cn.hutool.http.HttpGlobalConfig;
import cn.hutool.http.HttpRequest;
import cn.hutool.http.HttpResponse;
import cn.hutool.http.HttpUtil;
import cn.hutool.json.JSONUtil;
import com.lnold.yiwen.model.News;
import com.lnold.yiwen.model.entity.Detail;
import com.lnold.yiwen.model.entity.Simple;
import lombok.extern.slf4j.Slf4j;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.nio.charset.Charset;
import java.util.*;
import java.util.stream.Collectors;


@Slf4j
public class Utils {


    public static Detail getDetail(String channel, String url) {
        Detail details = new Detail();

        String html = HttpUtil.get(url);

        Document doc = Jsoup.parse(html);
        //*[@id="contain"]/div[2]/h1
        Elements title = doc.select("#contain > div.post_main > h1");
        Elements time = doc.select("#contain > div.post_main > div.post_info");
        details.setTitle(title.text());
        if (time.text().length() > 24) {
            details.setNewsFrom(CollectionUtil.getFirst(StrUtil.splitTrim(time.text().substring(24), " ")));
        } else {
            details.setNewsFrom(null);

        }
        Elements content = doc.select("#content > div.post_body");
        if (content.size() > 3) {
            for (int i = 3; i > 0; i--)
                content.remove(0);

        }
        details.setContent(content.toString());
        return details;
    }

    public static Detail mapToDetailDy(String channel, String url) {
        Detail details = new Detail();
        String html = HttpUtil.get(url);

        Document doc = Jsoup.parse(html);
        Elements title = doc.select(".article_title h2");
        Element from = doc.select(".share_box p span").get(2);
        Elements content = doc.select("#content p");

        details.setTitle(title.text());
        details.setNewsFrom(from.text());
        details.setContent(content.toString());
        return details;
    }

    public static Map<String, String> getUrlMap(){
        Map<String, String> urlMap = new LinkedHashMap<>();
        urlMap.put("war", "http://temp.163.com/special/00804KVA/cm_war.js?callback=data_callback");
        urlMap.put("sport", "http://sports.163.com/special/000587PR/newsdata_n_index.js?callback=data_callback");
        urlMap.put("tech", "http://tech.163.com/special/00097UHL/tech_datalist.js?callback=data_callback");
        urlMap.put("edu", "http://edu.163.com/special/002987KB/newsdata_edu_hot.js?callback=data_callback");
        //urlMap.put("ent", "http://ent.163.com/special/000380VU/newsdata_index.js?callback=data_callback");
        urlMap.put("money", "http://money.163.com/special/002557S5/newsdata_idx_index.js?callback=data_callback");
        urlMap.put("gupiao", "http://money.163.com/special/002557S6/newsdata_gp_index.js?callback=data_callback");
        urlMap.put("travel", "http://travel.163.com/special/00067VEJ/newsdatas_travel.js?callback=data_callback");
        urlMap.put("lady", "http://lady.163.com/special/00264OOD/data_nd_fashion.js?callback=data_callback");
        return urlMap;
    }

    public static List<News> getNewsList(String channel, String url) {
        log.info("获取新闻列表: {}", url);

        String content = httpGet(channel, url);
        if (StrUtil.isBlank(content)) {
            log.error("获取新闻列表失败");
            return new ArrayList<>();
        }


        // 提取data_callback() 内的数据
        content = content.substring(content.indexOf("(") + 1, content.lastIndexOf(")"));
        List<News> newsList = JSONUtil.toList(content, News.class);
        return newsList.stream()
                // 过滤掉非article类型的新闻, 其他类型的新闻格式不一样, 暂时不处理
                .filter(n -> "article".equals(n.getNewstype()))
                .peek(news -> news.setChannelname(channel))
                .collect(Collectors.toList());
    }

    private static String httpGet(String channel, String url) {
        HttpRequest request = HttpRequest.get(url).timeout(HttpGlobalConfig.getTimeout());
        HttpResponse response = request.execute();

        if ("war".equals(channel)) {
            response.charset(Charset.forName("GBK"));
        }

        return response.body();
    }

    public static List<Simple> transform(List<News> list) {
        List<Simple> simpleList = new ArrayList<>();
        for (News news : list) {
            Simple simple = new Simple();
            simple.setTitle(news.getTitle());
            simple.setDocurl(news.getDocurl());
            simple.setChannelname(news.getChannelname());
            simple.setTime(parseTime(news.getTime()));
            simple.setImgurl(news.getImgurl());
            simpleList.add(simple);
        }
        return simpleList;
    }

    public static Date parseTime(String time) {
        try {
            return DateUtil.parse(time, "MM/dd/yyyy HH:mm:ss");
        } catch (DateException e) {
            try {
                return DateUtil.parse(time, "yyyy-MM-dd HH:mm:ss");
            } catch (DateException e2) {
                log.error("时间转换失败");
            }
        }
        return null;
    }

}
