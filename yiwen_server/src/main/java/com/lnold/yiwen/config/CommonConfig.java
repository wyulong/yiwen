package com.lnold.yiwen.config;

import cn.hutool.extra.spring.SpringUtil;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

/**
 * CommonConfig
 *
 * @author wangyulong
 * @date 2023/10/21
 */
@Configuration
@Import(SpringUtil.class)
public class CommonConfig {
}
