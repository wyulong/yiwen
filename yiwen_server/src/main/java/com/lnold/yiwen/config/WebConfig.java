package com.lnold.yiwen.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.util.ResourceUtils;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

/**
 * WebConfig
 *
 * @author wangyulong
 * @date 2023/10/21
 */
@Configuration
public class WebConfig implements WebMvcConfigurer {

    /**
     * 资源处理器
     *
     * @param registry
     */
    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
        // swagger页面的资源
        registry.addResourceHandler("swagger-ui.html", "doc.html").addResourceLocations(ResourceUtils.CLASSPATH_URL_PREFIX + "/META-INF/resources/");
        registry.addResourceHandler("/webjars/**").addResourceLocations(ResourceUtils.CLASSPATH_URL_PREFIX + "/META-INF/resources/webjars/");

        // html
        registry.addResourceHandler("*").addResourceLocations(ResourceUtils.CLASSPATH_URL_PREFIX + "/static/");
    }


}
