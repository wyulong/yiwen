package com.lnold.yiwen.controller;

import com.lnold.yiwen.dao.SimpleMapper;
import com.lnold.yiwen.task.NewsTask;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;


@Api(tags = "管理接口")
@RestController
public class AdminController {

    @Autowired
    private NewsTask newsTask;
    @Autowired
    private SimpleMapper simpleMapper;

    @ApiOperation("手动触发Job")
    @GetMapping("/grabNews")
    public String grabNews() {
        newsTask.grabNews();
        return "got  news";
    }

    @ApiOperation("获取统计信息")
    @GetMapping("/count")
    public List<Map<String, Object>> count() {
        List<Map<String, Object>> maps = simpleMapper.countByChannelname();
        return maps;
    }


    @ApiOperation("抓取简介")
    @GetMapping("/grabSimple")
    public String grabSimple() {
        int count = newsTask.grabSimple();
        return "got " + count + " news";
    }


    @ApiOperation("抓取详情")
    @GetMapping("/grabDetail")
    public String grabDetail() {
        int count = newsTask.grabDetail();
        return "got " + count + " detail";
    }
}





