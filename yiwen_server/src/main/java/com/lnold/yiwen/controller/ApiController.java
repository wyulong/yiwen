package com.lnold.yiwen.controller;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.lnold.yiwen.model.entity.Detail;
import com.lnold.yiwen.model.entity.Simple;
import com.lnold.yiwen.service.DetailService;
import com.lnold.yiwen.service.SimpleService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@Api(tags = "新闻API接口")
@RestController
@RequestMapping("/api")
public class ApiController {

    @Autowired
    private SimpleService simpleService;
    @Autowired
    private DetailService detailService;



    @ApiOperation("获取新闻列表")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "type", dataType = "String", paramType = "query", defaultValue = "tech",
                     value = "新闻类型,war军事,sport体育,tech科技,edu教育,ent娱乐,money财经,gupiao股票,travel旅游,lady女人"),
            @ApiImplicitParam(name = "page", value = "页码", dataType = "Integer", paramType = "query", defaultValue = "1"),
            @ApiImplicitParam(name = "limit", value = "每页条数", dataType = "Integer", paramType = "query", defaultValue = "10")
    })
    @GetMapping("/simple")
    public Page<Simple> getList(@RequestParam("type") String type,
                                @RequestParam("page") Integer page,
                                @RequestParam("limit") Integer limit) {
        return simpleService.selectPage(type, page, limit);
    }


    @ApiOperation("获取新闻详情")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "simpleId", value = "新闻ID", dataType = "Long", paramType = "query")
    })
    @GetMapping("/detail")
    public Detail getDetail(@RequestParam("simpleId") Long simpleId) {
        return detailService.findBySimpleId(simpleId);
    }


}
