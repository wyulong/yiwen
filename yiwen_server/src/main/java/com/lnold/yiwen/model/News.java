package com.lnold.yiwen.model;

import lombok.Data;

import java.util.List;

@Data
public class News {

    private String title;
    private String digest;
    private String docurl;
    private String commenturl;
    private int tienum;
    private String tlastid;
    private String tlink;
    private String label;
    private List<Keywords> keywords;
    private String time;
    private String newstype;
    private List<Pics3> pics3;
    private String channelname;
    private String imgurl;
    private String add1;
    private String add2;
    private String add3;

}