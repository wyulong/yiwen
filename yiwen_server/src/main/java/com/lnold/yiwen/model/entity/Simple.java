package com.lnold.yiwen.model.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import lombok.Data;

import java.util.Date;

@TableName("news_simple")
@Data
public class Simple extends Model<Simple> {

    private Long id;
    private String title;
    private String docurl;
    private Date time;
    private String channelname;
    private String imgurl;
    private Boolean hasContent;

}
