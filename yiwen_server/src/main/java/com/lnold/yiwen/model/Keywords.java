package com.lnold.yiwen.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class Keywords {

    @JsonProperty("akey_link")
    private String akeyLink;
    private String keyname;

}