/**
  * Copyright 2016 bejson.com 
  */
package com.lnold.yiwen.model;

import lombok.Data;

@Data
public class Pics3 {

    private String pic;

}