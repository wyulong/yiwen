package com.lnold.yiwen.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.lnold.yiwen.model.entity.Detail;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface DetailMapper extends BaseMapper<Detail> {

}
