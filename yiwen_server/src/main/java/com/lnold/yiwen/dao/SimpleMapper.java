package com.lnold.yiwen.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.lnold.yiwen.model.entity.Simple;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

import java.util.List;
import java.util.Map;

@Mapper
public interface SimpleMapper extends BaseMapper<Simple> {


    @Select("select channelname,count(*) num from news_simple where has_content = true group by channelname")
    List<Map<String,Object>> countByChannelname();


}
