package com.lnold.yiwen;

import cn.hutool.extra.spring.SpringUtil;
import com.lnold.yiwen.task.NewsTask;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;


@Slf4j
@SpringBootApplication
public class ServerApplication implements CommandLineRunner {

    public static void main(String[] args) {
        SpringApplication.run(ServerApplication.class, args);
    }

    @Override
    public void run(String... args) {
        log.info("访问接口初始化数据： http://localhost:{}/grabNews",SpringUtil.getProperty("server.port"));
    }
}