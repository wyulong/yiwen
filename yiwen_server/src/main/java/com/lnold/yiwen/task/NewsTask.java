package com.lnold.yiwen.task;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.util.StrUtil;
import com.lnold.yiwen.model.News;
import com.lnold.yiwen.model.entity.Detail;
import com.lnold.yiwen.model.entity.Simple;
import com.lnold.yiwen.service.DetailService;
import com.lnold.yiwen.service.SimpleService;
import com.lnold.yiwen.util.Utils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

@Slf4j
@Component
public class NewsTask {

    @Autowired
    private SimpleService simpleService;
    @Autowired
    private DetailService detailService;

    @Scheduled(cron = "0 0/5 * * * ?")
    public void grabNews() {
        log.info("开始抓取数据");
        int count = grabSimple();
        log.info("抓取数据完成,新增{}条", count);
        int detailCount = grabDetail();
        log.info("抓取详情完成,新增{}条", detailCount);
    }

    public int grabSimple() {
        AtomicInteger count = new AtomicInteger();
        Utils.getUrlMap().forEach((channel, url) -> {
            log.info("开始抓取{}频道数据", channel);
            List<News> newsList = Utils.getNewsList(channel, url);
            List<Simple> simpleList = Utils.transform(newsList);
            simpleList.removeIf(simple -> simpleService.existsSimple(simple.getDocurl()));
            if (CollUtil.isNotEmpty(simpleList)) {
                simpleService.saveBatch(simpleList);
            }
            count.addAndGet(simpleList.size());
            log.info("抓取{}频道数据完成,新增{}条", channel, simpleList.size());
        });
        return count.get();
    }

    public int grabDetail() {
        int count = 0;
        List<Simple> simples = simpleService.selectNoContent();
        for (Simple simple : simples) {
            String docUrl = simple.getDocurl();
            if (!StrUtil.endWith(docUrl, "html")) {
                continue;
            }

            Detail detail = null;
            try {
                detail = Utils.getDetail(simple.getChannelname(), docUrl);
                if (StrUtil.isBlank(detail.getNewsFrom())) {
                    if (StrUtil.startWith(docUrl, "http://dy")) {
                        detail = Utils.mapToDetailDy(simple.getChannelname(), docUrl);
                    }
                }
            } catch (Throwable e) {
                log.warn("获取详情失败：{}", e.getMessage());
                continue;
            }

            Simple updateSimple = simpleService.getById(simple.getId());
            updateSimple.setHasContent(true);
            simpleService.updateById(updateSimple);

            detail.setSimpleId(simple.getId());
            detail.setTime(updateSimple.getTime());
            detailService.save(detail);

            count++;
        }
        return count;
    }


}
