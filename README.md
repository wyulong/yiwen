# 易闻天下 新闻客户端

## yiwen_app 
新闻客户端,使用html+css+js完成  
使用前端框架mui,官网：http://dev.dcloud.net.cn/mui/  
IDE:HBuilder，可实现云端打包成app。官网：http://dcloud.io/  
	
## yiwen_server
新闻服务端  
主要逻辑：服务端定时爬取新闻，存到数据库。
app请求时，服务端从数据库中查询数据，响应相应的新闻列表或新闻内容。
IDE:IDEA
