mui.init({
	swipeBack: true,
	statusBarBackground: '#f7f7f7',
	gestureConfig: {
		doubletap: true
	},
	subpages: [{
		url: 'list.html',
		id: 'list.html',
		styles: {
			top: '44px', //mui标题栏默认高度为45px；
			bottom: '0px' //默认为0px，可不定义；
		}
	}]
});
var main = null,
	menu = null,
	list = null;
var showMenu = false;
mui.plusReady(function() {
	//安装后首次打开显示引导页
	var isGuide = plus.storage.getItem("isGuide");
	if(!isGuide){
		mui.openWindow('guide.html','guide.html');
		plus.storage.setItem('isGuide',"true");
	}
	//仅支持竖屏显示
	plus.screen.lockOrientation("portrait-primary");
	main = plus.webview.currentWebview();
	list = plus.webview.getWebviewById('list.html');
	main.addEventListener('maskClick', closeMenu);
	//处理侧滑导航，为了避免和子页面初始化等竞争资源，延迟加载侧滑页面；
	setTimeout(function() {
		menu = mui.preload({
			id: 'index-menu',
			url: 'index-menu.html',
			styles: {
				left: 0,
				width: '50%',
				zindex: -1
			},
			show: {
				aniShow: 'none'
			}
		});
	}, 200);
});

var isInTransition = false;
/**
 * 显示侧滑菜单
 */
function openMenu() {
	if(isInTransition) {
		return;
	}
	if(!showMenu) {
		//侧滑菜单处于隐藏状态，则立即显示出来；
		isInTransition = true;
		menu.setStyle({
			mask: 'rgba(0,0,0,0)'
		}); //menu设置透明遮罩防止点击
		menu.show('none', 0, function() {
			//主窗体开始侧滑并显示遮罩
			main.setStyle({
				mask: 'rgba(0,0,0,0.4)',
				left: '50%',
				transition: {
					duration: 200
				}
			});
			mui.later(function() {
				isInTransition = false;
				menu.setStyle({
					mask: "none"
				}); //移除menu的mask
			}, 160);
			showMenu = true;
		});
	}
};
/**
 * 关闭菜单
 */
function closeMenu() {
	if(isInTransition) {
		return;
	}
	if(showMenu) {
		//关闭遮罩；
		//主窗体开始侧滑；
		isInTransition = true;
		main.setStyle({
			mask: 'none',
			left: '0',
			transition: {
				duration: 200
			}
		});
		showMenu = false;
		//等动画结束后，隐藏菜单webview，节省资源；
		mui.later(function() {
			isInTransition = false;
			menu.hide();
		}, 300);
	}
};
//点击左上角侧滑图标，打开侧滑菜单；
document.querySelector('.mui-icon-bars').addEventListener('tap', function(e) {
	console.log("icon-bars taped");
	if(showMenu) {
		closeMenu();
	} else {
		openMenu();
	}
});
//双击顶部导航，内容区回到顶部
document.querySelector('header').addEventListener('doubletap', function() {
	main.children()[0].evalJS('mui.scrollTo(0, 100)');
});
//主界面向右滑动，若菜单未显示，则显示菜单；否则不做任何操作
window.addEventListener("swiperight", openMenu);
//主界面向左滑动，若菜单已显示，则关闭菜单；否则，不做任何操作；
window.addEventListener("swipeleft", closeMenu);
//侧滑菜单触发关闭菜单命令
window.addEventListener("menu:close", closeMenu);
window.addEventListener("menu:open", openMenu);
//重写mui.menu方法，Android版本menu按键按下可自动打开、关闭侧滑菜单；
mui.menu = function() {
	if(showMenu) {
		closeMenu();
	} else {
		openMenu();
	}
}
//事件监听，更新导航栏标题
window.addEventListener("updateHeader", function(e) {
	var title = $('#title');
	title.empty();
	title.append(e.detail.title);
});