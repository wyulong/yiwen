// 初始化
mui.init({});

mui.ready(function(){
});
mui.plusReady(function() {
});
//清除图片原本样式，并增加自适应属性
function transImg() {
	$('#content').find('img').each(function() {
		var img = this;
		$(img).removeAttr('style');
		$(img).addClass("carousel-inner img-responsive img-rounded");
	})
	$('#content').find('a').each(function() {
		var a = this;
		$(a).removeAttr('href');
	})
}
	//添加newId自定义事件监听
window.addEventListener('newsId', function(event) {
	//获得事件参数
	var id = event.detail.id;
	$('.mui-card-header').empty();
	$('.mui-card-header').append('加载中...');
	$('#content').empty();
	plus.nativeUI.showWaiting();
	getDetail(id);
});


function getDetail(id) {
	var continer = $('#content');
	var header = $('.mui-card-header');
	mui.ajax('http://wangyi.butterfly.mopaasapp.com/detail/api?simpleId=' + id, {
		crossDomain: true,
		dataType: 'json', //服务器返回json格式数据
		type: 'get', //HTTP请求类型
		timeout: 10000, //超时时间设置为10秒；
		headers: {
			'Content-Type': 'application/json'
		},
		success: function(data) {
			//服务器返回响应，根据响应结果，分析是否登录成功；
			header.empty();
			header.append(data.title);
			var from=((data.from==null)?"未知":data.from);
			continer.append("<small>来源："+from+"<br>发布："+data.time.substring(0,16)+"</small>");
			continer.append(data.content);
			transImg();
			showList(header);
			showList(continer);
			plus.nativeUI.closeWaiting();
		},
		error: function(xhr, type, errorThrown) {
			plus.nativeUI.closeWaiting();
			mui.alert('网络异常，请稍候再试', '加载失败！', '确定', function(e) {
				mui.back();
			});
		}
	});

}

function showList(content) {
	if(content.is(':hidden')) content.show();
}
