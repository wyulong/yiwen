// 初始化
mui.init({
	preloadPages: [{
		id: 'detail.html',
		url: 'detail.html'
	}],
	pullRefresh: {
		container: '#refreshContainer', //待刷新区域标识，querySelector能定位的css选择器均可，比如：id、.class等
		up: {
			height: 50, //可选.默认50.触发上拉加载拖动距离
			auto: true, //可选,默认false.自动上拉加载一次
			contentrefresh: "正在加载...", //可选，正在加载状态时，上拉加载控件上显示的标题内容
			contentnomore: '没有更多数据了', //可选，请求完毕若没有更多数据时显示的提醒内容；
			callback: function() {
					var self = this; // 这里的this == mui('#refreshContainer').pullRefresh()
					// 加载更多的内容
					loadMore(self);
				} //必选，刷新函数，根据具体业务来编写，比如通过ajax从服务器获取新数据；
		}, //必选，刷新函数，根据具体业务来编写，比如通过ajax从服务器获取新数据；
		down: {
			height: 50, //可选,默认50.触发下拉刷新拖动距离,
			auto: false, //可选,默认false.自动下拉刷新一次
			contentdown: "下拉可以刷新", //可选，在下拉可刷新状态时，下拉刷新控件上显示的标题内容
			contentover: "释放立即刷新", //可选，在释放可刷新状态时，下拉刷新控件上显示的标题内容
			contentrefresh: "正在刷新...", //可选，正在刷新状态时，下拉刷新控件上显示的标题内容
			callback: refreshContent //必选，刷新函数，根据具体业务来编写，比如通过ajax从服务器获取新数据；
		}
	}
});

mui.plusReady(function() {
	var index = plus.webview.getWebviewById('index.html');
});

//更新新闻类型
window.addEventListener('updateContent', function(e) {
	type = e.detail.type;
	refreshContent();
});

//添加列表项的点击事件
mui('#content').on('tap', '.mui-card', function(e) {
	var detailPage = null;
	var id = this.getAttribute('id');
	//获得详情页面
	if(!detailPage) {
		detailPage = plus.webview.getWebviewById('detail.html');
	}
	//触发详情页面的newsId事件
	mui.fire(detailPage, 'newsId', {
		id: id
	});
	//右侧滑入300ms
	detailPage.show("slide-in-right", 300);
});
var page = 1;
var limit = 10;
var type = 'war';

function loadMore(pullRefresh) {
	getMore(type, page++, limit, pullRefresh);

}

function getMore(type, page, limit, pullRefresh) {
	var $ul = $('#content');
	mui.ajax('http://wangyi.butterfly.mopaasapp.com/news/api?type=' + type + '&page=' + page + '&limit=' + limit, {
		crossDomain: true,
		dataType: 'json', //服务器返回json格式数据
		type: 'get', //HTTP请求类型
		timeout: 10000, //超时时间设置为10秒；
		headers: {
			'Content-Type': 'application/json'
		},
		success: function(data) {
			//服务器返回响应，根据响应结果，分析是否登录成功；
			for(var i = 0; i < data.size; i++) {
				$ul.append(genLi(data.list[i]));
			}
			//返回数据数目小于请求数据数目，即没有更多数据
			if(parseInt(data.size) < parseInt(limit)) {
				pullRefresh.endPullupToRefresh(true);
			} else {
				pullRefresh.endPullupToRefresh(false);
			}
		},
		error: function(xhr, type, errorThrown) {
			mui.alert('网络异常，请稍候再试', '加载失败！', '确定', function(e) {
				plus.runtime.quit();
			});
		}
	});

	showContent($ul);
}

function refreshContent() {
	var $ul = $('#content');
	mui.ajax('http://wangyi.butterfly.mopaasapp.com/news/api?type=' + type + '&page=1' + '&limit=' + limit, {
		crossDomain: true,
		dataType: 'json', //服务器返回json格式数据
		type: 'get', //HTTP请求类型
		timeout: 10000, //超时时间设置为10秒；
		headers: {
			'Content-Type': 'application/json'
		},
		//清除已有数据列表，增加第一页数据，重置加载配置数据
		success: function(data) {
			//服务器返回响应，根据响应结果，分析是否登录成功；
			$ul.empty();
			for(var i = 0; i < data.size; i++) {
				$ul.append(genLi(data.list[i]));
			}
			mui('#refreshContainer').pullRefresh().endPulldownToRefresh();
			//下拉刷新完毕后，重置已加载数据
			mui('#refreshContainer').pullRefresh().refresh(true);
			page = 2;

			//}
		},
		error: function(xhr, type, errorThrown) {
			mui.alert('网络异常，请稍候再试', '加载失败！', '确定', function(e) {
			});
		}
	});
	showContent($ul);

}

function genLi(news) {
	var li = '<div class="mui-card" id=' + news.id + '><div class="mui-card-content"><div class="mui-card-content-inner"><div class="row"><div class="col-xs-4" style="color: #000000;">' +
		'<img  src="' +
		news.imgurl +
		'" /></div><div class="col-xs-8"><h4>' +
		news.title +
		'</h4><div class="row"><div class="col-xs-12"><p>' +
		news.time +
		'</p></div></div></div></div></div></div></div>';
	return li;
}

function showContent(con) {
	if(con.is(':hidden')) con.show();
}